<?php
function tinymce4_settings_form($form, $form_state){
	$form["exclude"] = array(
		"#type"=>"fieldset",
		"#title"=>t("Exclusion criteria"),
		"#description"=>t("Exclusion criterias"),
		"#tree"=>true,
		"#collapsible"=>true,
		"#collapsed"=>true
	);
	
	$paths = implode("\n",variable_get("tinymce4_exclude_paths", array()));
	$form["exclude"]["paths"] = array(
		"#type"=>"textarea",
		"#title"=>t("Paths"),
		"#description"=>t("Exclude tinymce from the following paths"),
		"#default_value"=>$paths
	);
	
	$form["actions"]["save"] = array(
		"#type"=>"submit",
		"#name"=>"btn-submit",
		"#value"=>t("Save")
	);
	
	return $form;
}

function tinymce4_settings_form_submit(&$form, &$form_state){
	$paths = explode("\n", $form_state["values"]["exclude"]["paths"]);
	variable_set("tinymce4_exclude_paths", $paths);
}