"use strict";
(function($){
	Drupal.behaviors.tinymce4 = {
		attach:function(context, settings){
			if(settings.tinymce4){
				if(settings.tinymce4.exclude && settings.tinymce4.exclude.paths){
					var current_path = document.location.pathname;
					var paths = settings.tinymce4.exclude.paths;
					var init = true;
					for(var i in paths){
						if(current_path.search(paths[i]) != -1){
							init = false;
							break;
						}
					}
					
					if(init){
						tinymce.init({ 
							selector:'textarea'
						});
					}

				}
			}
		}
	};
})(jQuery);